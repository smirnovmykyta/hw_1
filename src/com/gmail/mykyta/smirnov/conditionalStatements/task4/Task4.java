package com.gmail.mykyta.smirnov.conditionalStatements.task4;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Task4 {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите первое число");
        int a = Integer.parseInt(bufferedReader.readLine());

        System.out.println("Введите второе число");
        int b = Integer.parseInt(bufferedReader.readLine());

        System.out.println("Введите третье число");
        int c = Integer.parseInt(bufferedReader.readLine());

        System.out.println(getExpression(a, b, c));




    }

    public static int getExpression(int a, int b, int c){
        int d = a * b * c;
        int g = a + b + c;
        int m = Math.max(d, g);

        return m + 3;
    }
}
