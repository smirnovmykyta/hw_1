package com.gmail.mykyta.smirnov.conditionalStatements.task5;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Task5 {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите рейтинг студента");
        int a = Integer.parseInt(bufferedReader.readLine());

        System.out.println(getGrade(a));


    }

    public static  String getGrade(int a){
        if (a >= 0 && a < 20)
            return "Оценка студента F";

        if (a >= 20 && a < 40)
            return "Оценка студента E";

        if (a >= 40 && a < 60)
            return "Оценка студента D";

        if (a >= 60 && a < 75)
            return "Оценка студента C";

        if (a >= 75 && a < 90)
            return "Оценка студента B";

        if (a >= 90 && a <= 100)
            return "Оценка студента A";


        return "Рейтинг введён некорректно.";

    }
}
