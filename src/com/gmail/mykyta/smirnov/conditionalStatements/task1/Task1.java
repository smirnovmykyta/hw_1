package com.gmail.mykyta.smirnov.conditionalStatements.task1;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Task1 {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите число а");
        int a = Integer.parseInt(bufferedReader.readLine());

        System.out.println("Введите число б");
        int b = Integer.parseInt(bufferedReader.readLine());
        System.out.println(sumOrProduct(a,b));


    }

    public static int sumOrProduct(int a, int b){
        if (a % 2 == 0)
            return a * b;
        else
            return a + b;
    }
}
