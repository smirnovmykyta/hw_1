package com.gmail.mykyta.smirnov.conditionalStatements.task3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Task3 {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите первое число");
        int a = Integer.parseInt(bufferedReader.readLine());

        System.out.println("Введите второе число");
        int b = Integer.parseInt(bufferedReader.readLine());

        System.out.println("Введите третье число");
        int c = Integer.parseInt(bufferedReader.readLine());

        System.out.println("Сумма положительных чисел = " + sumPositiveNumbers(a, b, c));


    }

    public static int sumPositiveNumbers(int a, int b, int c){
        int sum = 0;

        if (a > 0)
            sum += a;

        if (b > 0)
            sum += b;

        if (c > 0)
            sum +=c;

        return sum;
    }
}
