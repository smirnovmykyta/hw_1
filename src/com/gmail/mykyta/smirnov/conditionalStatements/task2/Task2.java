package com.gmail.mykyta.smirnov.conditionalStatements.task2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Task2 {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите координату x");
        int x = Integer.parseInt(bufferedReader.readLine());

        System.out.println("Введите координату y");
        int y = Integer.parseInt(bufferedReader.readLine());
        System.out.println(pointInWhichQuarter(x,y));

    }

    public static String pointInWhichQuarter(int x, int y){
        if(x > 0 && y > 0)
            return "Точка принадлежит к первой четверти";
        else
        if(x < 0 && y > 0)
            return "Точка принадлежит к второй четверти";
        else
        if(x < 0 && y < 0)
            return "Точка принадлежит к третьей четверти";
        else
        if(x > 0 && y < 0)
            return "Точка принадлежит к четвёртой четверти";
        else
            return "Точка лежит на оси.";
    }

}
