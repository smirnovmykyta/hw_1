package com.gmail.mykyta.smirnov.cycle.task6;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Task6 {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите число n.");
        int number = Integer.parseInt(bufferedReader.readLine());

        System.out.println(reverse(number));

    }

    public static int reverse(int n){
        return Integer.valueOf(new StringBuilder(Integer.toString(n)).reverse().toString());
    }
}
