package com.gmail.mykyta.smirnov.cycle.task3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Task3 {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите число.");
        int number = Integer.parseInt(bufferedReader.readLine());

        System.out.println("Корень числа = " + sqrt(number));

    }

    public static int sqrt(int number) {
        int sqrtNum = (int) Math.round(Math.sqrt(number));
        return sqrtNum;
    }
}
