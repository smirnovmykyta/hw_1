package com.gmail.mykyta.smirnov.cycle.task2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Task2 {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите число.");
        int number = Integer.parseInt(bufferedReader.readLine());
        System.out.println(primeNumber(number));


    }

    public static String primeNumber(int number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {

                return "Число не является простым.";

            }
        }

        return "Число простое.";
    }
}
