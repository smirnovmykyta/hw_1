package com.gmail.mykyta.smirnov.cycle.task1;

import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {

        System.out.println("Сумма и количество четных чисел = " + Arrays.toString(sumAndAmount()));

    }

    public static int[] sumAndAmount() {
        int sum = 0;
        int number = 0;

        for (int i = 1; i < 100; i++) {
            if (i % 2 == 0) {
                sum += i;
                number++;
            }
        }
        return new int[]{sum, number};
    }
}
