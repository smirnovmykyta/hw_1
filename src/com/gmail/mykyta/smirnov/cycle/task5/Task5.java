package com.gmail.mykyta.smirnov.cycle.task5;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Task5 {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите число n.");
        String s = bufferedReader.readLine();

        System.out.println("Сумма цифр числа = " + sum(s));

    }

    public static int sum(String s) {
        String[] a = s.split("");
        int sum = 0;

        for (int i = 0; i < a.length; i++) {
            sum += Integer.parseInt(a[i]);
        }
        return sum;
    }
}
