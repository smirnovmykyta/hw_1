package com.gmail.mykyta.smirnov.functionTest.task5;

import com.gmail.mykyta.smirnov.function.task5.Task5;
import org.junit.Assert;
import org.junit.Test;

public class TestTask5 {

    @Test
    public void test_numberToString(){
        long number = 2561;
        String expected = "два тысяч пятьсот шестьдесят один";

        String actual = Task5.numberToString(number);

        Assert.assertEquals(expected,actual);
    }
}
