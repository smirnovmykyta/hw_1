package com.gmail.mykyta.smirnov.functionTest.task4;

import com.gmail.mykyta.smirnov.function.task4.Task4;
import org.junit.Assert;
import org.junit.Test;

public class TestTask4 {

    @Test
    public void test_distance(){
        double expected = 5.656854249492381;
        double delta = 0.0;

        double actual = Task4.distance(2, 5 ,6 ,1);

        Assert.assertEquals(expected, actual, delta);
    }
}
