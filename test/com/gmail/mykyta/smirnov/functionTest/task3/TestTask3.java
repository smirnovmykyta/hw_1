package com.gmail.mykyta.smirnov.functionTest.task3;

import com.gmail.mykyta.smirnov.function.task3.Task3;
import com.siyeh.ig.encapsulation.AssignmentOrReturnOfFieldWithMutableTypeInspection;
import org.junit.Assert;
import org.junit.Test;

public class TestTask3 {

    @Test
    public void test_wordToNumber(){
        String input = "пятьсот двадцать один";
        long expected = 521;

        long actual = Task3.wordToNumber(input);

        Assert.assertEquals(expected, actual);
    }
}

