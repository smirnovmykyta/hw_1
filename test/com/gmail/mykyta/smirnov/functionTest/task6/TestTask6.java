package com.gmail.mykyta.smirnov.functionTest.task6;

import com.gmail.mykyta.smirnov.function.task6.Task6;
import org.junit.Assert;
import org.junit.Test;

public class TestTask6 {

    @Test
    public void test_wordToNumber(){
        String input = "два миллиона девятьсот восемьдесят один тысяча шестьсот двадцать шесть ";
        long expected = 2981626;

        long actual = Task6.wordToNumber(input);

        Assert.assertEquals(expected, actual);
    }
}
