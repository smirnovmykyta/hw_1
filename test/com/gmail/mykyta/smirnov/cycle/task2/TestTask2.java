package com.gmail.mykyta.smirnov.cycle.task2;

import com.gmail.mykyta.smirnov.cycle.task2.Task2;
import org.junit.Assert;
import org.junit.Test;

public class TestTask2 {
    @Test
    public void testPrimeNumber() {
        String temp = Task2.primeNumber(5);
        Assert.assertEquals("Число простое.", temp);
    }
}
