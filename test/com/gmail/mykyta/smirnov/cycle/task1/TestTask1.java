package com.gmail.mykyta.smirnov.cycle.task1;

import com.gmail.mykyta.smirnov.cycle.task1.Task1;
import org.junit.Assert;
import org.junit.Test;

public class TestTask1 {

    @Test
    public void testSumAndAmount(){

        Assert.assertArrayEquals(new int[]{2450, 49},Task1.sumAndAmount());

    }
}
