package com.gmail.mykyta.smirnov.cycle.task5;

import com.gmail.mykyta.smirnov.cycle.task5.Task5;
import org.junit.Assert;
import org.junit.Test;

public class TestTask5 {
    @Test
    public void testSum(){
        int temp = Task5.sum("12345");
        Assert.assertEquals(15,temp);
    }
}
