package com.gmail.mykyta.smirnov.cycle.task3;

import com.gmail.mykyta.smirnov.cycle.task3.Task3;
import org.junit.Assert;
import org.junit.Test;

public class TestTask3 {
    @Test
   public void testSqrt(){
        int temp = Task3.sqrt(9);
        Assert.assertEquals(3,temp);
    }
}
