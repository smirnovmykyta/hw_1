package com.gmail.mykyta.smirnov.cycle.task6;

import com.gmail.mykyta.smirnov.cycle.task6.Task6;
import org.junit.Assert;
import org.junit.Test;

public class TestTask6 {
    @Test
    public void testReverse(){
        int temp = Task6.reverse(123);
        Assert.assertEquals(321, temp);
    }
}
