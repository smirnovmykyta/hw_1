package com.gmail.mykyta.smirnov.conditionalStatementsTest.task1;

import com.gmail.mykyta.smirnov.conditionalStatements.task1.Task1;
import org.junit.Assert;
import org.junit.Test;

public class TestTask1 {

    @Test
    public void testTask1Product(){
       int temp = Task1.sumOrProduct(2,4);
       Assert.assertEquals(8,temp);
    }

    @Test
    public void testTask1Sum(){
        int temp = Task1.sumOrProduct(1,3);
        Assert.assertEquals(4,temp);
    }
}
