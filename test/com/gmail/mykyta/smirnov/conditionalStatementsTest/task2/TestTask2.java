package com.gmail.mykyta.smirnov.conditionalStatementsTest.task2;

import com.gmail.mykyta.smirnov.conditionalStatements.task2.Task2;
import org.junit.Assert;
import org.junit.Test;

public class TestTask2 {

    @Test
    public void testFirstQuarter() {
        String temp = Task2.pointInWhichQuarter(2, 4);
        Assert.assertEquals("Точка принадлежит к первой четверти", temp);
    }

    @Test
    public void testSecondQuarter() {
        String temp = Task2.pointInWhichQuarter(-1, 2);
        Assert.assertEquals("Точка принадлежит к второй четверти", temp);
    }

    @Test
    public void testThirdQuarter() {
        String temp = Task2.pointInWhichQuarter(-1, -4);
        Assert.assertEquals("Точка принадлежит к третьей четверти", temp);
    }

    @Test
    public void testFourthQuarter() {
        String temp = Task2.pointInWhichQuarter(4, -1);
        Assert.assertEquals("Точка принадлежит к четвёртой четверти", temp);
    }

    @Test
    public void testCoordinateAxis() {
        String temp = Task2.pointInWhichQuarter(0, 2);
        Assert.assertEquals("Точка лежит на оси.", temp);
    }
}
