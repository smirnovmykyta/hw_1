package com.gmail.mykyta.smirnov.conditionalStatementsTest.task5;

import com.gmail.mykyta.smirnov.conditionalStatements.task5.Task5;
import org.junit.Assert;
import org.junit.Test;

public class TestTask5 {

    @Test
    public void testGradeF(){
        String temp = Task5.getGrade(10);
        Assert.assertEquals("Оценка студента F", temp);
    }

    @Test
    public void testGradeE(){
        String temp = Task5.getGrade(30);
        Assert.assertEquals("Оценка студента E", temp);
    }

    @Test
    public void testGradeD(){
        String temp = Task5.getGrade(50);
        Assert.assertEquals("Оценка студента D", temp);
    }

    @Test
    public void testGradeC(){
        String temp = Task5.getGrade(70);
        Assert.assertEquals("Оценка студента C", temp);
    }

    @Test
    public void testGradeB(){
        String temp = Task5.getGrade(80);
        Assert.assertEquals("Оценка студента B", temp);
    }

    @Test
    public void testGradeA(){
        String temp = Task5.getGrade(95);
        Assert.assertEquals("Оценка студента A", temp);
    }

    @Test
    public void testGradeFail(){
        String temp = Task5.getGrade(105);
        Assert.assertEquals("Рейтинг введён некорректно.", temp);
    }
}
