package com.gmail.mykyta.smirnov.conditionalStatementsTest.task4;

import com.gmail.mykyta.smirnov.conditionalStatements.task4.Task4;
import org.junit.Assert;
import org.junit.Test;

public class TestTask4 {

    @Test
    public void testGetExpressionSum(){
        int temp = Task4.getExpression(1,1,1);
        Assert.assertEquals(6,temp);
    }

    @Test
    public void testGetExpredionProduct(){
        int temp = Task4.getExpression(1, 2, 3);
        Assert.assertEquals(9, temp);
    }
}
