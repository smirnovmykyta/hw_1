package com.gmail.mykyta.smirnov.conditionalStatementsTest.task3;

import com.gmail.mykyta.smirnov.conditionalStatements.task3.Task3;
import org.junit.Assert;
import org.junit.Test;

public class TestTask3 {

    @Test
    public void testSumPositiveNumbers(){
        int temp = Task3.sumPositiveNumbers(2, 5, 6);
        Assert.assertEquals(13, temp);
    }
}
