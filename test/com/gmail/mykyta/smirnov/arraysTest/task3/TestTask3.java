package com.gmail.mykyta.smirnov.arraysTest.task3;

import com.gmail.mykyta.smirnov.arrays.task3.Task3;
import org.junit.Assert;
import org.junit.Test;

public class TestTask3 {

    @Test
    public void testIndexMinNumber(){
        int actual = Task3.indexMinNumber(new int[] {5,9,4,3,6});
        Assert.assertEquals(3,actual);
    }
}
