package com.gmail.mykyta.smirnov.arraysTest.task2;

import com.gmail.mykyta.smirnov.arrays.task2.Task2;
import org.junit.Assert;
import org.junit.Test;

public class TestTask2 {

    @Test
    public void testMax(){
        int actual = Task2.max(new int[]{4, 5, 2, 7, 1}) ;
        Assert.assertEquals(7,actual);
    }
}
