package com.gmail.mykyta.smirnov.arraysTest.task10;

import com.gmail.mykyta.smirnov.arrays.task10.HeapSort;

import com.gmail.mykyta.smirnov.arrays.task10.MergeSort;
import com.gmail.mykyta.smirnov.arrays.task10.QuickSort;
import com.gmail.mykyta.smirnov.arrays.task10.ShellSort;
import org.junit.Assert;
import org.junit.Test;

public class TestTask10 {

    @Test
    public void test_heapSort(){
        int[] actual = HeapSort.heapSort(new int[] {2,5,6,8,4,9,3});

        Assert.assertArrayEquals(new int[]{2,3,4,5,6,8,9},actual);
    }

    @Test
    public void test_mergeSort(){
        int[] actual = new int[]{2,5,6,8,4,9,3};
        MergeSort obMergeSort = new MergeSort();

        obMergeSort.mergeSort(actual,actual.length);

        Assert.assertArrayEquals(new int[]{2,3,4,5,6,8,9}, actual);
    }

    @Test
    public void test_quickSort(){
        int[] actual = new int[]{2,5,6,8,4,9,3};
        QuickSort obQuickSort = new QuickSort();

        obQuickSort.quickSort(actual, 0, actual.length - 1);

        Assert.assertArrayEquals(new int[]{2,3,4,5,6,8,9}, actual);
    }

    @Test
    public void test_shellSort(){
        int[] actual = new int[] {2,5,6,8,4,9,3};
        ShellSort obShellSort = new ShellSort();

        obShellSort.sortShell(actual);

        Assert.assertArrayEquals(new int[]{2,3,4,5,6,8,9}, actual);
    }
}
