package com.gmail.mykyta.smirnov.arraysTest.task8;

import com.gmail.mykyta.smirnov.arrays.task8.Task8;
import org.junit.Assert;
import org.junit.Test;

public class TestTask8 {

    @Test
    public void change(){
        int actual[] = Task8.change(new int[] {1,2,3,4,5});
        Assert.assertArrayEquals(new int[] {3,4,5,1,2}, actual);
    }
}
