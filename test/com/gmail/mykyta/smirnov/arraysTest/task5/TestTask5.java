package com.gmail.mykyta.smirnov.arraysTest.task5;

import com.gmail.mykyta.smirnov.arrays.task5.Task5;
import org.junit.Assert;
import org.junit.Test;

public class TestTask5 {
    @Test
    public void sumNumberWithOddIndex(){
        int actual = Task5.sumNumberWithOddIndex(new int[]{1,2,3,4,5});
        Assert.assertEquals(6,actual);
    }
}
