package com.gmail.mykyta.smirnov.arraysTest.task6;

import com.gmail.mykyta.smirnov.arrays.task6.Task6;
import org.junit.Assert;
import org.junit.Test;

public class TestTas6 {

    @Test
    public void revers(){
        int actual[] = Task6.reverse(new int[] {1,2,3,4,5});
        Assert.assertArrayEquals(new int[]{5,4,3,2,1}, actual);
    }
}
