package com.gmail.mykyta.smirnov.arraysTest.task1;

import com.gmail.mykyta.smirnov.arrays.task1.Task1;
import org.junit.Assert;
import org.junit.Test;

public class TestTask1 {

    @Test
    public void testMin(){
        int actual = Task1.min(new int[] {4,5,2,7,1}) ;
        Assert.assertEquals(1,actual);
    }
}
