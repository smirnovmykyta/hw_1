package com.gmail.mykyta.smirnov.arraysTest.task4;

import com.gmail.mykyta.smirnov.arrays.task4.Task4;
import org.junit.Assert;
import org.junit.Test;

public class TestTask4 {

    @Test
    public void testIndexMaxNumber(){
        int actual = Task4.indexMaxNumber(new int[] {5,6,8,3,2,9});
        Assert.assertEquals(5,actual);
    }
}
