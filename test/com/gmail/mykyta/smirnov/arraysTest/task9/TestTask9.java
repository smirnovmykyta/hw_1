package com.gmail.mykyta.smirnov.arraysTest.task9;

import com.gmail.mykyta.smirnov.arrays.task9.Task9;
import org.junit.Assert;
import org.junit.Test;

public class TestTask9 {

    @Test
    public void bubbleSort(){
        int[] actual = Task9.bubble(new int[] {2,5,6,8,4,9,3});
        Assert.assertArrayEquals(new int[]{2,3,4,5,6,8,9},actual);
    }

    @Test
    public void selectSort(){
        int[] actual = Task9.bubble(new int[] {2,5,6,8,4,9,3});
        Assert.assertArrayEquals(new int[]{2,3,4,5,6,8,9},actual);
    }

    @Test
    public void insertSort(){
        int[] actual = Task9.bubble(new int[] {2,5,6,8,4,9,3});
        Assert.assertArrayEquals(new int[]{2,3,4,5,6,8,9},actual);
    }
}
