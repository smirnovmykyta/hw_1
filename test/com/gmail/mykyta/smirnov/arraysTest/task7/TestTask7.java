package com.gmail.mykyta.smirnov.arraysTest.task7;

import com.gmail.mykyta.smirnov.arrays.task7.Task7;
import org.junit.Assert;
import org.junit.Test;

public class TestTask7 {

    @Test
    public void amountOddElement(){
        int actual = Task7.amountOddElement(new int[] {1,2,3,4,5});
        Assert.assertEquals(3,actual);
    }
}
